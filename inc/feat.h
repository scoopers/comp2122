#ifndef FEAT_H
#define FEAT_H

void currentDir();
void changeDir(char** args);
void executeProcess(char** args, int bg);
void help();

#endif
