#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "../inc/data.h"
#include "../inc/init.h"
#include "../inc/feat.h"

// array of possible commands to compare to
const char *commands[7] = {"info", "exit", "pwd", "cd", "ex", "exb", "help"};

// sets up a prompt to show the user they are inside the shell
void init() {
  printf("*****************************************\n");
  printf("*    Shell Program - Input a command    *\n");
  printf("*****************************************\n\n");
}

// using standard input, accepts a string form the user
char* input() {
  // print prompt
  printf(GREEN "%s" RESET "$ ", cwd);
  // clear output buffer
  fflush(stdout);
  // variable creation to hold a temporary buffer and a final string
  char* input = NULL;
  char* temp = NULL;
  size_t size = 0;
  size_t index = 0;

  // default to EOF for loop
  int cha = EOF;

  while(cha) {
    // get a character from keyboard input
    cha = getc(stdin);

    // make character input zero if unusable input specified (for later condition)
    if(cha == EOF || cha == '\n') {
      cha = 0;
    }

    // rellalocate memory of string to match new length with added char
    if(size >= index) {
      size += INC;
      temp = realloc(input, size);
      if(!temp) {
        free(input);
        input = NULL;
        break;
      }
      input = temp;
    }
    // assign character to build string
    input[index] = cha;
    index++;
  }
  return input;
}

char** splitInput(char* cmd) {
  // buffer size
  int buff = BUFF;
  // index
  int index = 0;
  // pointer to pointers of arguments (will become set of strings)
  char** args = malloc(buff * sizeof(char*));
  // a single argument
  char* arg;

  // initial memory check
  if(!args) {
    printf("Memory allocation error\n");
    exit(1);
  }

  // next string after delimiter
  arg = strtok(cmd, DELIM);
  // allocate next token to next pointer space
  while(arg != NULL) {
    args[index] = arg;
    index++;
    // increase buffer size if exceeds and reallocate memory
    if(index <= buff) {
      buff += BUFF;
      args = realloc(args, buff * sizeof(char*));
      if(!args) {
        printf("Memory allocation error\n");
      }
    }
    // reset variable for next argument
    arg = strtok(NULL, DELIM);
  }
  // next index is null
  args[index] = NULL;
  return args;
}

// interpret first argument (command) and return corresponding choice
int interpret(char** args) {
  int count = 7;
  char* cmd = args[0];
  if(cmd == NULL) {
    return 0;
  }
  // compare each command in array to user input
  for (size_t i = 0; i < count; i++) {
    if(strcmp(cmd, commands[i]) == 0) {
      return i + 1;
    }
  }
  return -1;
}

// accepts a command, inteprets and calls the corresponding function
void prompt() {
  char** args = NULL;
  int q = 1;
  while(q) {
    currentDir();
    char* cmd = input();
    args = splitInput(cmd);
    int choice = interpret(args);
    switch (choice) {
      case 0:
        // for no proper input (e.g. \n)
        break;
      case 1:
        printf("COMP2211 Simplified Shell by ll15s3c\n");
        break;
      case 2:
        // assign to stopping condition to break the loop
        q = 0;
        break;
      case 3:
        //print current directory
        printf("%s\n", cwd);
        break;
      case 4:
        changeDir(args);
        break;
      case 5:
        executeProcess(args, 0);
        break;
      case 6:
        executeProcess(args, 1);
        break;
      case 7:
        help();
        break;
      default:
        // invalid command
        printf("please enter a valid command: use 'help' for assistance\n");
        break;
    }
    free(cmd);
  }
  return;
}

// entry point for the program
int main(int argc, char const *argv[]) {
  init();
  prompt();
  return 0;
}
