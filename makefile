# # # # # # # # # # # # # # #
# General Purpose Makefile  #
# # # # # # # # # # # # # # #

# Shell Makefile #

# MACROS #

# Project Name #
PROJECT = shell
# Compiler #
CC = gcc
CFLAGS = -g -std=c99
LIBS = -lm

# Directories #
# Source directory #
SDIR = src
# Header file directory
IDIR = inc
# Build directory #
BDIR = build
# Object file directory
ODIR = $(BDIR)/obj
	dummy_build_folder := $(shell mkdir -p $(ODIR))

# Dependencies #
_DEPS =  init.h feat.h data.h # put all header file names used here
# Dependencies + Location
DEPS = $(patsubst %, $(IDIR)/%, $(_DEPS))

# Objects #
_OBJ =  main.o feat.o # put all desired object file names here
# Objects + Location
OBJ = $(patsubst %, $(ODIR)/%, $(_OBJ))

# Compilation #
$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

# Linking #
$(BDIR)/$(PROJECT): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

# Clean #
.PHONY: clean
clean:
	rm -rf $(BDIR)
