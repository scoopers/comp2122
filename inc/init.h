#ifndef FUNC_H
#define FUNC_H

void init();
char* input();
char** splitInput();
int interpret(char** args);
void prompt();


#endif
