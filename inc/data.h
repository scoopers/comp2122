#ifndef DATA_H
#define DATA_H

#include "linux/limits.h"

// ansi escape sequence codes for text manipulation
#define GREEN "\x1b[32m"
#define RED "\x1b[31m"
#define RESET "\x1b[0m"
#define BOLD "\e[1m"

#define INC 64
#define BUFF 64
#define DELIM " \t\r\n\a"

char cwd[PATH_MAX];

#endif
