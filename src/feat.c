#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "string.h"
#include "fcntl.h"
#include "sys/types.h"
#include "sys/wait.h"
#include "../inc/data.h"
#include "../inc/feat.h"

// get current directory
void currentDir() {
  if(getcwd(cwd, sizeof(cwd)) == NULL) {
    printf("error\n");
  }
  return;
}

// change process to directory of path provided
void changeDir(char** args) {
  // if too many arguments provided print message and return
  if(args[2] != NULL) {
    printf("Too many arguments\n");
    return;
  }

  if(args[1] == NULL) {
    printf("Please enter a path\n");
  } else {
    if(chdir(args[1])) {
      perror("Error");
    }
  }
  return;
}

// execute given process with arguments
void executeProcess(char** args, int bg) {

  /* FILENAME MANIPULATION */
  //remove first element in args (command)
  int redirNum = 0;
  size_t i;
  for (i = 0; args[i+1] != NULL; i++) {
    args[i] = args[i+1];
    // if command contains > make it and next argument null
    if(strcmp(args[i], ">") == 0) {
      redirNum = i+1;
      args[i] = NULL;
    }
  }
  // make duplicate argument null
  if(redirNum == 0) {
    args[i] = NULL;
  }

  /* PROCESS */
  pid_t pid, wpid;
  int status;

  int fd;
  // check if output needs redirecting
  if((redirNum != 0) && (args[redirNum+1] != NULL)) {
    fd = open(args[redirNum + 1], O_WRONLY);
    // if error opening file
    if(fd < 0) {
      perror("Error");
    }
  }

  // create process and handle errors
  pid = fork();
  if(pid == 0) {
    // close stdout so instead can write to file
    if(redirNum > 0) {
      close(1);
      dup(fd);
    }
    // child process creation
    if(execvp(args[0, args) == -1) {
      perror("Error");
    }
    // exit process
    exit(1);
  } else if(pid < 0) {
    perror("Error");
    // wait on process if argument is 0
  } else if(bg == 0) {
    do {
      if(redirNum > 0) {
        close(fd);
      }
      wpid = waitpid(pid, &status, WUNTRACED);
    } while(!WIFEXITED(status) && !WIFSIGNALED(status));
  } else {
    // print pid of process in background
    printf("Process [%d]\n", pid);
  }
  return;
}

// displays help information about commands
void help() {
  printf(BOLD RED "info    " RESET " - displays info about shell\n");
  printf(BOLD RED "pwd     " RESET " - displays absolute path to current directory\n");
  printf(BOLD RED "cd PATH " RESET " - changes working directory to PATH specified\n");
  printf(BOLD RED "ex PATH " RESET " - executes program at PATH specfied\n");
  printf(BOLD RED "exb PATH" RESET " - executes program in background at PATH specified\n");
  printf(BOLD RED "help    " RESET " - displays info about commands\n");
}
